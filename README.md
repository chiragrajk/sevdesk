
## Installing

```shell
npm install sevdesk
```

## Debuging
```
DEBUG=sevdesk:client npm run COMMAND
```

## Instantiate client
```javascript
const Sevdesk = require('sevdesk')
const sevdesk = new Sevdesk({ apiKey: 'abc' })
```

To change the base url
```javascript
const sevdesk = new Sevdesk({ apiKey: 'abc', baseUrl: 'https://a-url' })
```

## Usage
```javascript
sevdesk.contact.get(options)
  .then(results => {
    console.log(results)
  })
  .catch(error => {
    console.log(error)
  })
```

or with callbacks

```javascript
sevdesk.contact.get(function(err, results) {
  if (err) { console.error(err) }
  console.log(results);
})

sevdesk.contact.get(options,function(err, results) {
  if (err) { console.error(err) }
  console.log(results);
})
```

## Available methods
### Contact
```javascript
sevdesk.contact.get(options, callback)
sevdesk.contact.create(data, callback)
sevdesk.contact.update(id, data, callback)
sevdesk.contact.delete(id, callback)
sevdesk.contact.getNextCustomerNumber(options, callback)
```

### ContactAddress
```javascript
sevdesk.contactAddress.get(options, callback)
sevdesk.contactAddress.create(data, callback)
sevdesk.contactAddress.update(id, data, callback)
sevdesk.contactAddress.delete(id, callback)
```

