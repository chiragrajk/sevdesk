const util = require('./util')

class Base {
  constructor (client, base) {
    this.client = client
    this.base = base
    this.basePath = `/${this.base}/`
  }
  
  get (options, cb) {
    [options, cb] = util.checkOptions(options, cb)

    return this.client._request({
      method: 'GET',
      path: this.basePath,
      qs: options
    }, cb)
  }

  create (data, cb) {
    return this.client._request({
      method: 'POST',
      path: this.basePath,
      qs: data
    }, cb)
  }

  update (id, data, cb) {
    return this.client._request({
      method: 'PUT',
      path: this.basePath + id,
      qs: data
    }, cb)
  }

  delete (id, cb) {
    return this.client._request({
      method: 'DELETE',
      path: this.basePath + id
    }, cb)
  }
}

module.exports = Base
