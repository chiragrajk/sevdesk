const Base = require('./base')

const BASE_NAME = 'Category'

class Category extends Base{
  constructor (client) {
    super(client, BASE_NAME)
  }
}

module.exports = Category
