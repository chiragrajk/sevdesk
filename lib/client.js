const _ = require('lodash')
const EventEmitter = require('events').EventEmitter
const request = require('request-promise')
const debug = require('debug')('sevdesk:client')

const Contact = require('./contact')
const ContactAddress = require('./contactAddress')
const CommunicationWay = require('./communicationWay')
const OrderPosition = require('./orderPosition')
const Order = require('./order')
const Invoice = require('./invoice')
const Discounts = require('./discounts')
const Feed = require('./feed')
const Category = require('./category')
const Static = require('./static')

class Client extends EventEmitter {
  constructor (options={}) {
    super()
    this.qs = {}
    this.setAuth(options)
    this.baseUrl = options.baseUrl || 'https://my.sevdesk.de/api/v1'
    this.on('apiCall', params => debug('apiCall', params))

    this.contact = new Contact(this)
    this.contactAddress = new ContactAddress(this)
    this.communicationWay = new CommunicationWay(this)
    this.orderPosition = new OrderPosition(this)
    this.order = new Order(this)
    this.invoice = new Invoice(this)
    this.discounts = new Discounts(this)
    this.feed = new Feed(this)
    this.category = new Category(this)
    this.static = new Static(this)
  }

  setAuth (options = {}) {
    if (options.apiKey) {
      this.auth = { 'Authorization': options.apiKey }
    }
  }

  _request (opts, cb = () => {}) {
    const params = _.cloneDeep(opts)
    if (this.auth) { params.headers = this.auth }
    params.json = true
    params.url = this.baseUrl + params.path
    delete params.path
    params.qs = Object.assign({}, this.qs, params.qs)

    params.qsStringifyOptions = {
      arrayFormat: 'repeat'
    }

    this.emit('apiCall', params)
    return request(params)
      .then(body => {
        cb(null, body.objects)
        return body.objects
      })
      .catch(err => {
        cb(err)
        throw(err)
      })
  }

}

module.exports = Client
module.exports.default = Client 