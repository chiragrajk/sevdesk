const Base = require('./base')

const BASE_NAME = 'CommunicationWay'

class CommunicationWay extends Base {
  constructor (client) {
    super(client, BASE_NAME)
  }
}

module.exports = CommunicationWay