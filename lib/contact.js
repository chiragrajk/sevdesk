const Base = require('./base')
const util = require('./util')

const BASE_NAME = 'Contact'

class Contact extends Base {
  constructor (client) {
    super(client, BASE_NAME)
  }

  getNextCustomerNumber (options, cb) {
    [options, cb] = util.checkOptions(options, cb)
    
    return this.client._request({
      method: 'GET',
      path: this.basePath + 'Factory/getNextCustomerNumber',
      qs: options
    }, cb)
  }

  getAddresses (id, options, cb) {
    [options, cb] = util.checkOptions(options, cb)

    return this.client._request({
      method: 'GET',
      path: this.basePath + id + '/getAddresses',
      qs: options
    }, cb)
  }
}

module.exports = Contact