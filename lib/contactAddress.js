const Base = require('./base')

const BASE_NAME = 'ContactAddress'

class ContactAddress extends Base{
  constructor (client) {
    super(client, BASE_NAME)
  }
}

module.exports = ContactAddress
