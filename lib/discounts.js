const Base = require('./base')

const BASE_NAME = 'Discounts'

class Discounts extends Base{
  constructor (client) {
    super(client, BASE_NAME)
  }
}

module.exports = Discounts
