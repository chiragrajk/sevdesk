const Base = require('./base')

const BASE_NAME = 'Feed'

class Feed extends Base{
  constructor (client) {
    super(client, BASE_NAME)
  }
}

module.exports = Feed
