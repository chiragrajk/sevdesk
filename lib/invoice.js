const Base = require('./base')
const util = require('./util')

const BASE_NAME = 'Invoice'

class Invoice extends Base{
  constructor (client) {
    super(client, BASE_NAME)
  }

  sendViaEmail (id, data, cb) {
    return this.client._request({
      method: 'POST',
      path: this.basePath + id + '/sendViaEmail',
      qs: data
    }, cb)
  }

  getPdf (id, options, cb) {
    [options, cb] = util.checkOptions(options, cb)

    return this.client._request({
      method: 'GET',
      path: this.basePath + id + '/getPdf',
      qs: options
    }, cb)
  }

  sendViaEmail (data, cb) {
    return this.client._request({
      method: 'POST',
      path: this.basePath + 'Factory/createInvoiceFromOrder',
      qs: data
    }, cb)
  }
}

module.exports = Invoice
