const Base = require('./base')
const util = require('./util')

const BASE_NAME = 'Order'

class Order extends Base{
  constructor (client) {
    super(client, BASE_NAME)
  }

  sendViaEmail (id, data, cb) {
    return this.client._request({
      method: 'POST',
      path: this.basePath + id + '/sendViaEmail',
      qs: data
    }, cb)
  }

  // api not found on swagger api
  // TODO: Order/Factory/createPackingListFromOrder

  getPdf (id, options, cb) {
    [options, cb] = util.checkOptions(options, cb)

    return this.client._request({
      method: 'GET',
      path: this.basePath + id + '/getPdf',
      qs: options
    }, cb)
  }

  getNextOrderNumber (options, cb) {
    [options, cb] = util.checkOptions(options, cb)

    return this.client._request({
      method: 'GET',
      path: this.basePath + 'Factory/getNextOrderNumber',
      qs: options
    }, cb)
  }
}

module.exports = Order
