const Base = require('./base')

const BASE_NAME = 'OrderPos'

class OrderPosition extends Base{
  constructor (client) {
    super(client, BASE_NAME)
  }
}

module.exports = OrderPosition
