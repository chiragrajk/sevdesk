const util = require('./util')

class Static {
  constructor (client, base) {
    this.client = client
  }

  country (options, cb) {
    [options, cb] = util.checkOptions(options, cb)

    return this.client._request({
      method: 'GET',
      path: '/StaticCountry',
      qs: options
    }, cb)
  }

  industry (options, cb) {
    [options, cb] = util.checkOptions(options, cb)

    return this.client._request({
      method: 'GET',
      path: '/StaticIndustry',
      qs: options
    }, cb)
  }
  
  referralProgram (options, cb) {
    [options, cb] = util.checkOptions(options, cb)

    return this.client._request({
      method: 'GET',
      path: '/StaticReferralProgram',
      qs: options
    }, cb)
  }
  
}

module.exports = Static