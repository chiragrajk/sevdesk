function checkOptions(options, cb) {
  if (typeof options === 'function') {
    return [{}, options]
  } else {
    return [options, cb]
  }
}

module.exports = {
  checkOptions
}