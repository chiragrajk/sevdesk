const chai = require('chai')
const expect = chai.expect

const Sevdesk = require('..')

describe('client', function () {
  let sevdesk

  describe('apiKey', function () {
    before(() => {
      sevdesk = new Sevdesk({apiKey: 'demo'})
    })

    it('should instance all methods', function () {
      expect(sevdesk.contact).to.be.an('object')
      expect(sevdesk.contactAddress).to.be.an('object')
      expect(sevdesk.communicationWay).to.be.an('object')
      expect(sevdesk.orderPosition).to.be.an('object')
      expect(sevdesk.order).to.be.an('object')
      expect(sevdesk.invoice).to.be.an('object')
      expect(sevdesk.discounts).to.be.an('object')
      expect(sevdesk.feed).to.be.an('object')
      expect(sevdesk.category).to.be.an('object')
    })
  })
})